<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    public function createCustomUser($username, $password) : int
    {
        $userId = DB::table('users')->insertGetId(
            [
                'username' => $username,
                'password' => bcrypt($password),
                'remember_token' => Str::random(10),
            ]
        );

        return $userId;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // user_id 1
        $this->createCustomUser('JohnDoe', 'ActivityManagement');
    }
}
