## About Activity Management

Activity Management is a simple project for management of activities

### User login

Run the migrations and the user seeder

Log in with the following credentials:
 - username: JohnDoe
 - password: ActivityManagement

### User Activities
Create activities
Edit activity
Delete activity

### Activity Report
Display all activities for an appropriate time interval (date-from date-to)
Send the report to email with a unique link

### Print Report
Using barryvdh/laravel-dompdf
Print report with date for every day in selected interval and total time spent on any activities in that interval for logged user

### Versions
Laravel v8.76.2
PHP v7.4.7
