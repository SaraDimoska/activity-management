@if (session('success'))

    <div class="alert alert-success">
        <button class="close" data-dismiss="alert">x</button>
        {{session('success')}}
    </div>

@elseif (session('error'))

    <div class="alert alert-danger">
        <button class="close" data-dismiss="alert">x</button>
        {{session('error')}}
    </div>

@elseif (session('info'))

    <div class="alert alert-warning">
        <button class="close" data-dismiss="alert">x</button>
        {!!session('info')!!}
    </div>

@endif