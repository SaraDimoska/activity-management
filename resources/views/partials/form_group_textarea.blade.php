<div class="form-group">
    <label>{{$label}}</label>
    <textarea name="{{$name}}" id="{{$id}}" cols="30" rows="10" class="form-control" value="{{$value ?? ''}}" {{$appendix ?? ''}}>{{$content ?? ''}}</textarea>
</div>