<div class="form-group" id="{{$divId ?? ''}}">
    <label>{{$label}}</label>
    <input type="{{$type ?? 'text'}}" class="form-control" name="{{$name}}" value="{{$value ?? ''}}" {{$appendix ?? ''}} step="{{$step ?? ''}}">
</div>