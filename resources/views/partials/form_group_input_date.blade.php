<label>{{$label}}</label>
<div class="form-group">
    <input type="date" name="{{$name}}" value="{{$value ?? ''}}" class="form-control" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-mask-format="0000-00-00" autocomplete="off" {{$appendix ?? ''}}>
</div>