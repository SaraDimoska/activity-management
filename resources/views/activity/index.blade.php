@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-3 d-flex justify-content-between">
                <div>
                    <h3>{{ __('Activities') }}</h3>
                </div>
                <div class="btn-toolbar">
                    <div class="mr-1">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#create-activity-modal">Add activity</button>
                    </div>
                    <div class="mr-1">
                        <button class="btn btn-info" data-toggle="modal" data-target="#send-report-modal">Send Report</button>
                    </div>
                    <form action="{{route('print.report')}}" method="POST">
                        @csrf
                        <input type="hidden" name="start_date" value="{{$weekStartDate}}">
                        <input type="hidden" name="end_date" value="{{$weekEndDate}}">
                        <button type="submit" class="btn btn-info">Print Report</button>
                    </form>
                </div>
            </div>
            @include('partials.error_handling')
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <form action="{{route('activity.index')}}" method="GET" class="d-flex">
                        <div style="width: 40%; margin-right: 10px;">
                            <label for="start-date">Start Date</label>
                            <input type="date" name="start_date" value="{{$weekStartDate}}" class="form-control" id="start-date" required>
                        </div>
                        <div style="width: 40%; margin-right: 10px;">
                            <label for="end-date">End Date</label>
                            <input type="date" name="end_date" value="{{$weekEndDate}}" class="form-control" id="end-date" required>
                        </div>
                        <div class="align-self-end float-right ml-auto">
                            <button type="submit" class="btn btn-primary">Go</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($activities as $activity)
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="py-3">
                    <div class="card">
                        <div class="card-body card-activity-body">
                            <h4>Activity Date</h4>
                            <p>{{$activity->activity_date}}</p>
                            <h4>Time Spent</h4>
                            <p>{{$activity->time_spent}} h</p>
                            <h4>Description</h4>
                            <p>{{$activity->description}}</p>
                        </div>
                        <?php $activity->description = str_replace(array("\n", "\r"), ' ', $activity->description); ?>
                        <div class="card-footer">
                            <button class="btn btn-info" onclick="editActivityModal('{{route('activity.update', $activity->id)}}', '{{$activity->activity_date}}', '{{$activity->time_spent}}', '{{$activity->description}}')">Edit</button>
                            <button class="btn btn-danger" onclick="deleteActivityModal('{{route('activity.destroy', $activity->id)}}', '{{$activity->id}}')">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
            @empty
                <div class="mt-3">
                    <h5>There are no activities!</h5>
                </div>
            @endforelse
        </div>
    </div>

    
@endsection
<x-modals.create_activity/>
<x-modals.edit_activity />
<x-modals.delete_activity />
<x-modals.send_activity_report :weekStartDate="$weekStartDate" :weekEndDate="$weekEndDate"/>


