<x-modals.create_modal>
    <x-slot name="id">delete-activity-modal</x-slot>
    <x-slot name="title">Delete activity</x-slot>
    <x-slot name="body">
        Are you sure you want to delete this activity?
    </x-slot>
    <x-slot name="footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" data-dismiss="modal">Cancel</button>
        <form action="" method="POST">
            @method('DELETE')
            @csrf
            <input type="hidden" name="activity_id">
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    </x-slot>
    <x-slot name="script">
        <script>
            function deleteActivityModal(action, activityId)
            {

                $('#delete-activity-modal form').attr('action', action);

                $('#delete-activity-modal input[name=activity_id]').val(activityId);

                $('#delete-activity-modal').modal('show');
            }
        </script>
    </x-slot>
</x-modals.create_modal>
