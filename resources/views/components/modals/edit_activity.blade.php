<x-modals.create_modal>
    <x-slot name="id">edit-activity</x-slot>
    <x-slot name="title">Edit Activity</x-slot>
    <x-slot name="body">
        <form action="" method="POST" id="edit-activity-form">
            @csrf
            @method('PUT')

             {{-- Activity Date --}}
             @include('partials.form_group_input_date', ['label' => 'Activity Date', 'name' => 'activity_date', 'required'])

             {{--  Time Spent --}}
             @include('partials.form_group_input', ['type' => 'number', 'label' => 'Time Spent', 'name' => 'time_spent', 'required', 'step' => '0.1'])
 
             {{-- Description --}}
             @include('partials.form_group_textarea', ['label' => 'Description', 'name' => 'description', 'id' => 'description'])

            {{-- Submit --}}
            <button type="submit" class="btn btn-primary mb-2">Update</button>
        </form>
    </x-slot>
    <x-slot name="script">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
        <script>
            $('#edit-activity-form').validate({
                rules: {
                    activity_date: 'required',
                    time_spent: {
                        required: true,
                        number: true
                    },
                    description: 'required'
                }
            })
        </script>
        <script>
            function editActivityModal(action, activityDate, timeSpent, description)
            {
                $('#edit-activity form').attr('action', action);

                $('#edit-activity input[name=activity_date]').val(activityDate);
                $('#edit-activity input[name=time_spent]').val(timeSpent);
                $('#edit-activity textarea').val(description);

                $('#edit-activity').modal('show');
            }
        </script>
    </x-slot>
</x-modals.create_modal>
