<x-modals.create_modal>
    <x-slot name="id">create-activity-modal</x-slot>
    <x-slot name="title">Create activity</x-slot>
    <x-slot name="body">
        <form action="{{route('activity.store')}}" method="POST" id="create-activity-form">
            @csrf

            {{-- Activity Date --}}
            @include('partials.form_group_input_date', ['label' => 'Activity Date', 'name' => 'activity_date', 'required'])

            {{--  Time Spent --}}
            @include('partials.form_group_input', ['type' => 'number', 'label' => 'Time Spent', 'name' => 'time_spent', 'required', 'step' => '0.1'])

            {{-- Description --}}
            @include('partials.form_group_textarea', ['label' => 'Description', 'name' => 'description', 'id' => 'description', 'required'])

            {{-- Submit --}}
            <button type="submit" class="btn btn-primary mb-2">Create</button>

        </form>
    </x-slot>
    <x-slot name="script">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
        <script>
            $('#create-activity-form').validate({
                rules: {
                    activity_date: 'required',
                    time_spent: {
                        required: true,
                        number: true
                    },
                    description: 'required'
                },
                messages: {
                    time_spent: {
                        required: 'This field is required and numeric.'
                    }
                }
            })
        </script>
    </x-slot>
</x-modals.create_modal>
