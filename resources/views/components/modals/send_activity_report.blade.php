<x-modals.create_modal>
    <x-slot name="id">send-report-modal</x-slot>
    <x-slot name="title">Send Report</x-slot>
    <x-slot name="body">
        <form action="{{route('email.report')}}" method="POST">
            @csrf

            {{-- Email address --}}
            @include('partials.form_group_input', ['type' => 'email', 'label' => 'Email address', 'name' => 'email', 'required'])

            <input type="hidden" name="start_date" value="{{$weekStartDate}}">
            <input type="hidden" name="end_date" value="{{$weekEndDate}}">

            {{-- Submit --}}
            <button type="submit" class="btn btn-primary mb-2">Send Email</button>

        </form>
    </x-slot>
    <x-slot name="script">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
        <script>
            $('#send-report-modal form').validate({
                rules: {
                    email: 'required',
                }
            })
        </script>
    </x-slot>
</x-modals.create_modal>
