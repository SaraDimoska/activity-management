@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @forelse ($activities as $activity)
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="p-2">
                <div class="card card-activity-body">
                    <div class="card-body">
                        <h4>Activity Date</h4>
                        <p>{{$activity->activity_date}}</p>
                        <h4>Time Spent</h4>
                        <p>{{$activity->time_spent}} h</p>
                        <h4>Description</h4>
                        <p>{{$activity->description}}</p>
                    </div>
                </div>
            </div>
        </div>
        @empty
            <h5>There are no activities!</h5>
        @endforelse
    </div>
</div>
@endsection
