<?php

namespace App\Http\Controllers;

use Carbon\CarbonPeriod;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Auth;

class PrintUserReportController extends Controller
{
    public function generatePDF(Request $request)
    {
        $activities = UserActivity::where([
            'user_id' => Auth::id(),
        ])->whereBetween('activity_date', [$request->start_date, $request->end_date])->get();

        $interval = CarbonPeriod::create($request->start_date, $request->end_date);

        $userActivityData = [];

        $totalSumForTheInterval = 0;
        foreach ($interval as $date) 
        {
            $activityDateSum = 0;
            foreach($activities as $activity)
            {
                if($activity->activity_date === $date->format('Y-m-d'))
                {
                    $activityDateSum += $activity->time_spent;
                }
            }
            $totalSumForTheInterval += $activityDateSum;
            $userActivityData[$date->format('d/m/y')] = $activityDateSum;
        }
          
        $pdf = PDF::loadView('activity.print-report', [
            'userActivityData' => $userActivityData,
            'totalSumForTheInterval' => $totalSumForTheInterval
        ]);

        $output = $pdf->output();

        return new Response($output, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  'inline; filename="UserReport.pdf"',
        ]);
    
    }
}
