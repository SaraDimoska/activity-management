<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use Carbon\CarbonPeriod;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Http\Requests\ActivityRequest;
use App\Http\Requests\SendReportByEmailRequest;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $weekStartDate = now()->startOfWeek()->format('Y-m-d');
        $weekEndDate = now()->endOfWeek()->format('Y-m-d');

        if($request->start_date && $request->end_date)
        {
            $weekStartDate = $request->start_date;
            $weekEndDate = $request->end_date;
        }

        $activities = auth()->user()->activities()->whereBetween('activity_date', [$weekStartDate, $weekEndDate])->get();

        return view('activity.index', [
            'activities' => $activities,
            'weekStartDate' => $weekStartDate,
            'weekEndDate' => $weekEndDate
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActivityRequest $request)
    {
        UserActivity::create($request->validated() + [
            'user_id' => Auth::id(),
        ]);

        return redirect(route('activity.index'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActivityRequest $request, $id)
    {
        $userActivity = UserActivity::find($id);
        $userActivity->update($request->validated());

        return redirect(route('activity.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userActivity = UserActivity::find($id)->delete();

        if($userActivity)
        {
            return redirect(route('activity.index'))->with('success', 'Activity deleted.');
        } 
        else 
        {
            return redirect(route('activity.index'))->with('error', 'Something went wrong.');
        }

    }

    public function activityReportForGuest($senderId, $startDate, $endDate)
    {
        $user = User::find(Crypt::decrypt($senderId));
        $activities = $user->activities()->whereBetween('activity_date', [Crypt::decrypt($startDate), Crypt::decrypt($endDate)])->get();

        return view('report_for_guest', ['activities' => $activities]);
    }

}
