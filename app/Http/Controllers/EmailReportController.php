<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ActivityReportMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

class EmailReportController extends Controller
{
    public function sendReportByEmail(Request $request)
    {
        $user = Auth::user();
        $recipient = $request->get('email');

        Mail::send('emails.send_report', [
            'email' => $request->get('email'),
            'user' => $user,
            'url' => route('report.for.guest', [Crypt::encrypt($user->id), Crypt::encrypt($request->start_date), Crypt::encrypt($request->end_date)])
        ],
        function ($message) use ($recipient, $request) {
            $message->from('fortestpurpose.email@gmail.com');
            $message->to($recipient)
                ->subject('Activity Report');
        });

        return back()->with('success', 'The report was sent.');
    }
}
