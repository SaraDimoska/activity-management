<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\EmailReportController;
use App\Http\Controllers\PrintUserReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/report/guest/{senderId}/{startDate}/{endDate}', [ActivityController::class, 'activityReportForGuest'])->name('report.for.guest');

Route::group(['middleware' => 'auth'], function(){
    Route::post('/email/report', [EmailReportController::class, 'sendReportByEmail'])->name('email.report');
    Route::post('/print-report', [PrintUserReportController::class, 'generatePDF'])->name('print.report');
    Route::resource('activity', ActivityController::class, ['except' => ['create', 'show', 'edit']]);
});
